import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { NavbarComponent } from './core/navbar.component';
import { SyncStorage } from './core/store/catalog/products/products.actions';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, NavbarComponent],
  template: `
 
    <app-navbar />
    <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {
}
