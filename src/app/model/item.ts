// model/item.ts
export interface Item {
  id: number;
  name: string;
  cost: number;
  categoryId: number;
}
