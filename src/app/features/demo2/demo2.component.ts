// demo-cs.component.ts
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ItemsStore} from "./store/items.store";

@Component({
  selector: 'app-demo-2',
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>Demo Component Store</h1>

    <h1>{{categoryId$ | async | json}}</h1>
    <button (click)="filterByCategoryId(1)">1 shoes</button>
    <button (click)="filterByCategoryId(2)">2 TShirt</button>
    <button (click)="filterByCategoryId(null)">All</button>
    
    <li *ngFor="let item of (items$ | async)">
      {{ item.name }} € {{item.cost}}
    </li>

    <pre>{{items$ | async | json}}</pre>

  `,
  providers: [ItemsStore],
})
export default class Demo2Component {
  //readonly items$ = this.componentStore.select(state => state.items);
  items$ = this.itemsStore.filteredItems$;
  categoryId$ = this.itemsStore.categoryId$;

  constructor(
    private itemsStore: ItemsStore
  ) {
    this.itemsStore.getItemsByCatId(null)
  }

  filterByCategoryId(id: number | null) {
    // SOLUTION 1
    // this.itemsStore.setState(state => ({ ...state, categoryFilter: id}))
    // SOLUTION 2
    // this.itemsStore.patchState({ categoryFilter: id})
    // SOLUTION 3: required updater method
    this.itemsStore.getItemsByCatId(id)
    // this.itemsStore.changeCategoryFilter(id)
  }


}
