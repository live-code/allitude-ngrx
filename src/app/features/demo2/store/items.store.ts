// store/items.store.ts
import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import {ComponentStore} from "@ngrx/component-store";
import { catchError, EMPTY, iif, Observable, of, switchMap, tap } from 'rxjs';
import { Item } from '../../../model/item';

export interface ItemsState {
  products: Item[];
  categoryId: number | null
}

const initialState: ItemsState = {
  products: [
    // { id: 1, name: 'Nike Air', cost: 19, categoryId: 1 },
    // { id: 2, name: 'Adidas XYZ', cost: 120, categoryId: 1 },
    // { id: 3, name: 'DC NY Shirt', cost: 29, categoryId: 2 },
    // { id: 3, name: 'Puma Shirt', cost: 29, categoryId: 2 },
  ],
  categoryId: null
}

@Injectable()
export class ItemsStore extends ComponentStore<ItemsState> {
  http = inject(HttpClient)
  constructor() {
    // super({products: []});
    super(initialState);
  }

  readonly items$ = this.select(state => state.products);
  readonly categoryId$ = this.select(state => state.categoryId);

  readonly filteredItems$ = this.select(
    this.items$,
    this.categoryId$,
    (items, categoryId) => items.filter(item => {
      if (categoryId === null)
        return !!item;

      return item.categoryId === categoryId
    })
  )

  /*changeCategoryFilter(id: number | null) {
    this.updater((state, id) => {

    })
  }*/

  getItemsByCatId = this.effect((catId$: Observable<number | null>) => {
    return catId$.pipe(
      // switchMap(catId => this.http.get<Item[]>(catId ? `http://localhost:3000/items/?categoryId=${catId}` : `http://localhost:3000/items/`)
      switchMap(catId => {
        const url = catId ?
          `http://localhost:3000/items/?categoryId=${catId}` :
          `http://localhost:3000/items/`
        ;
        return this.http.get<Item[]>(url)
          .pipe(
            //👇 Act on the result within inner pipe.
            tap({
              next: (products) => this.patchState({ products }),
              error: (e) => console.log(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
      }),
    )
  })

  getItemsByCatId2 = this.effect((catId$: Observable<number | null>) => {
    return catId$.pipe(
      // switchMap(catId => this.http.get<Item[]>(catId ? `http://localhost:3000/items/?categoryId=${catId}` : `http://localhost:3000/items/`)
      switchMap(catId => {
        const url = catId ?
          `http://localhost:3000/items/?categoryId=${catId}` :
          `http://localhost:3000/items/`
        ;
        return iif(
          () => !!catId,
          this.http.get<Item[]>(url)
            .pipe(
              //👇 Act on the result within inner pipe.
              tap({
                next: (products) => this.patchState({ products }),
                error: (e) => console.log(e),
              }),
              // 👇 Handle potential error within inner pipe.
              catchError(() => EMPTY),
            ),
          this.http.get<Item[]>(url)
            .pipe(
              //👇 Act on the result within inner pipe.
              tap({
                next: (products) => this.patchState({ products }),
                error: (e) => console.log(e),
              }),
              // 👇 Handle potential error within inner pipe.
              catchError(() => EMPTY),
            )
        )

      }),
    )
  })


  changeCategoryFilter = this.updater((state, categoryId: number | null) => {
    return {
      ...state,
      categoryId
    };
  })


  clear() {
    this.patchState({ products: []})
    // this.setState({ products: [], categoryId: null})
    //this.items$.setState({products: []});

  }
}

