import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterLinkActive } from '@angular/router';
import { IsSignedDirective } from '../../shared/directives/is-signed.directive';
import { PaddingDirective, PaddingSize } from '../../shared/directives/padding.directive';
import { ThemeDirective } from '../../shared/directives/theme.directive';
import { UrlDirective } from '../../shared/url.directive';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [CommonModule, UrlDirective, PaddingDirective, ThemeDirective, FormsModule, RouterLinkActive, IsSignedDirective],
  template: `
    <h1>Directive Examples</h1>
    
    <button
      [appPadding]="padding"
      showIcons
      [total]="3"
      totalItems="3"
      appUrl="https://www.fabiobiondi.dev">my website</button>
    
    
    <button (click)="padding ='sm'">sm</button>
    <button (click)="padding ='xl'">xl</button>
    
    <input type="text" [ngModel]>
  `,

  styles: [
  ]
})
export default class Demo3Component {
  padding: PaddingSize = 'xl';
}
