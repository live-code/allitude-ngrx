import { Component, Type, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LatLngExpression } from 'leaflet';
import { CardComponent } from '../shared/componensts/card.component';
import { LeafletComponent } from '../shared/componensts/leaflet.component';
import { LoaderComponent, LoaderProps } from '../shared/componensts/loader.component';
import { PanelComponent } from '../shared/componensts/panel/panel.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [CommonModule, LoaderComponent, LeafletComponent],
  template: `
    
    <app-leaflet [coords]="coords" [zoom]="zoom"/>
  
    <button (click)="coords = [43, 13]">ita</button>
    <button (click)="coords = [30, 13]">altro</button>
    <button (click)="zoom  = zoom +1">+</button>
    
    <app-loader
      *ngFor="let cfg of configs"
      [cfg]="cfg"
      (onEvent)="doSomething($event)"
    >
    </app-loader>
    
    <hr>

    <app-loader [cfg]="dynamicComponent" />
    
    <pre>{{dynamicComponent | json}}</pre>
    
    <button (click)="dynamicComponent = configs[0]">0</button>
    <button (click)="dynamicComponent = configs[1]">1</button>

    <ng-template
      *ngComponentOutlet="componentToLoad.component; inputs: componentToLoad.inputs"
    ></ng-template>
    
    <button (click)="loadCompo2()">load compo</button>
  `,
})
export default class Demo6Component {
  zoom = 5;
  coords: LatLngExpression = [41, 11]
  doSomething(evtData: { compo: string, event: string }) {
    console.log('parent', evtData)
  }
  configs: any = [
    {
      component: 'card',
      inputs: {
        title: 'ciao',
        icon: '♥️'
      },
      events: ['iconClick']
    },
    {
      component: 'panel',
      inputs: {
        variants: 'primary',
      }
    },
  ]
  dynamicComponent = this.configs[0]


  componentToLoad: Widget =   {
    component: CardComponent,
    inputs: {
      title: 'CON COMPONENT OUTLET',
      icon: '♥️'
    },
  }

  loadCompo2()  {
    this.componentToLoad = {
      component: PanelComponent,
      inputs: {
        variants: 'primary',
      }
    }

  }


}

export type Widget = {
  component: Type<any>,
  inputs: any
}
