import { Component, ElementRef, inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from '../core/auth.service';
import { BoxComponent } from '../shared/componensts/box.component';
import { CardComponent } from '../shared/componensts/card.component';
import { PanelBodyComponent } from '../shared/componensts/panel/panel-body.component';
import { PanelIconComponent } from '../shared/componensts/panel/panel-icon.component';
import { PanelTitleComponent } from '../shared/componensts/panel/panel-title.component';
import { PanelComponent } from '../shared/componensts/panel/panel.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [CommonModule, CardComponent, BoxComponent, PanelComponent, PanelComponent, PanelTitleComponent, PanelBodyComponent, PanelIconComponent],
  template: `
    <h1>Login Simulation</h1>
    
    
    <button (click)="authSrv.login()">Login</button>
    <button (click)="authSrv.logout()">Logout</button>
    
    <pre>{{authSrv.auth$ | async | json}}</pre>
    
    <app-card 
      title="ciao" 
      icon="💩"
      (iconClick)="doSomething()"
    >
      <form>
        <input type="text">
        <input type="text">
        <input type="text">
        <input type="text">
      </form> 
    </app-card>

    <hr>
    
    <app-card 
      title="miao" 
      icon="♥️"
      (iconClick)="doSomething2()"
    >
      <app-box />
      
    </app-card>

    <br>
    <br>
    <br>
    
    <app-panel 
      variants="secondary"
      (iconClick)="doSomething2()"
    >
      <app-panel-icon>💩</app-panel-icon>
      <app-panel-title font="thin">TITOLO</app-panel-title>
      <app-panel-body>bla bla....</app-panel-body>
    </app-panel>

  `,
})
export default class Demo4Component {
  @ViewChild('msgError') msgError!: TemplateRef<any>

  authSrv = inject(AuthService)
  view = inject(ViewContainerRef)

  doSomething() {
    console.log('do something')
  }

  doSomething2() {
    console.log('do something2')
  }
}
