import { Component, Query, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AccordionGroupComponent } from '../shared/componensts/accordion/accordion-group.component';
import { AccordionComponent } from '../shared/componensts/accordion/accordion.component';
import { CardComponent } from '../shared/componensts/card.component';
import { ColComponent } from '../shared/componensts/grid/col.component';
import { RowComponent } from '../shared/componensts/grid/row.component';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [CommonModule, FormsModule, AccordionGroupComponent, CardComponent, AccordionComponent, RowComponent, ColComponent],
  template: `
    <pre>visible {{visible}}</pre>
    
    <app-card 
      title="card title"
      [(opened)]="visible"
    >
      blabla
    </app-card>
    
    <app-row [grow]="1">
      <app-col>1</app-col>
      <app-col>2</app-col>
      <app-col>3</app-col>
    </app-row>

    <app-row>
      <app-col>1</app-col>
      <app-col>2</app-col>
      <app-col>3</app-col>
    </app-row>

    <div class="flex justify-between">
      <div class="bg-red-400 grow ">1</div>
      <div class="bg-green-400 grow">2</div>
      <div class="bg-orange-400 grow">3</div>
    </div>
    
    
    <app-accordion icon="💩">
      <app-accordion-group 
        title="accord group" 
      >
        bla bla...
      </app-accordion-group>
      
      <app-accordion-group 
        title="acc group 2"
      >
        bla bla...
      </app-accordion-group>

      <app-accordion-group
        title="acc group 2"
      >
        bla bla...
      </app-accordion-group>

      <app-accordion-group
        title="acc group 2"
      >
        bla bla...
      </app-accordion-group>
    </app-accordion>
  `,
})
export default class Demo5Component {

  visible = false;

  value = 'hello text'

}
