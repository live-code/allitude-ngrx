import { Component, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ProductsActiveActions } from '../core/store/catalog/products-active/products-active.actions';
import { ProductsActions } from '../core/store/catalog/products/products.actions';
import { getProducts, getProductsError, getSomething } from '../core/store/catalog/products/products.selectors';
import { selectUrl } from '../core/store/router/router.selector';
import { Product } from '../model/product';
import { StopPropagationDirective } from '../shared/stop-propagation.directive';
import { UrlDirective } from '../shared/url.directive';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [CommonModule, StopPropagationDirective, UrlDirective],
  template: `
    <h1>NGRX DEMO</h1>


    <div *ngIf="productsError$ | async">Ahia!</div>
    <li 
      *ngFor="let p of products$ | async" 
      (click)="selectProduct(p)"
    >
      {{p.name}}
      
      <button appStopPropagation (click)="deleteProduct(p.id!)">DELETE</button>
      <!--<button appStopPropagation appUrl="http://www.google.com">VISITA</button>-->
    </li>
    
    url is: {{url$ | async}}
  `,
})
export default class Demo1Component implements OnInit {
  store = inject(Store)
  products$: Observable<Product[]> = this.store.select(getProducts)
  productsError$ = this.store.select(getProductsError)
  something = this.store.select(getSomething)

  url$ = this.store.select(selectUrl)

  ngOnInit(): void {
    this.store.dispatch(ProductsActions.load())
  }

  selectProduct(item: Product) {
    this.store.dispatch(ProductsActiveActions.setActive({ item }))
  }

  deleteProduct(id: number) {
    this.store.dispatch(ProductsActions.delete({ id }))
  }

}
