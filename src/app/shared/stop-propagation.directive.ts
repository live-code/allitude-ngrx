import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appStopPropagation]',
  standalone: true
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  clickMe(event: MouseEvent) {
    event.stopPropagation()
  }

}
