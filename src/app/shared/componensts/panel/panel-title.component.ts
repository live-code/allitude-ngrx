import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-panel-title',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div [ngClass]="{
      'font-bold': font === 'bold',
      'font-thin': font === 'thin'
    }" >
      <ng-content></ng-content>
    </div>
  `,
})
export class PanelTitleComponent {
  @Input() font: 'bold' | 'thin' = 'bold'
}
