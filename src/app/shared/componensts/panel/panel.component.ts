import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div>
      <div 
        class="text-white flex justify-between p-3"
        [ngClass]="{
          'bg-sky-500': variants === 'primary',
          'bg-slate-200 text-black': variants === 'secondary',
        }"
      >
        <ng-content select="app-panel-title"></ng-content>
        <ng-content select="app-panel-icon"></ng-content>
      </div>
      
      <ng-content></ng-content>
    </div>

  `,
  styles: [`
    .title {
      background: black ; color: white
    }
  `]
})
export class PanelComponent {
  @Input() variants: 'primary' | 'secondary' = 'primary'


}
