import { Component, HostBinding, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RowComponent } from './row.component';

@Component({
  selector: 'app-col',
  standalone: true,
  imports: [CommonModule],
  template: `
    <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get class() {
    return this.rowComponent.grow === 0 ?
      'bg-red-500' : 'grow bg-orange-200'
  }
  rowComponent = inject(RowComponent);
}
