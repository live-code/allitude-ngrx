import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-row',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="flex gap-2 justify-between">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class RowComponent {
  @Input() grow = 0;
}
