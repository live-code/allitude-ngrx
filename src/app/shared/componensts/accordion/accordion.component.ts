import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChild,
  ContentChildren, Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="border-2 border-black">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(AccordionGroupComponent) groups!: QueryList<AccordionGroupComponent>
  @Input() icon = ''

  ngAfterContentInit() {
    this.groups.toArray()[0].opened = true;
    this.groups.toArray()
      .forEach(g => {
        g.toggle.subscribe(() => {
          this.closeAll();
          g.opened = true
        })
      })
  }

  closeAll() {
    this.groups.toArray()
      .forEach(g => {
        g.opened = false
      })
  }
}
