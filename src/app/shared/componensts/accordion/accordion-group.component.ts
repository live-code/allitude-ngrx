import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionComponent } from './accordion.component';

@Component({
  selector: 'app-accordion-group',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div>
      <div 
        class="bg-green-400 text-white flex justify-between p-3"
        (click)="toggle.emit()"
      >
        <div>{{accordion.icon}} {{title}}</div>
      </div>
      <div *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>

  `,
  styles: [`
    .title {
      background: black ; color: white
    }
  `]
})
export class AccordionGroupComponent {
  @Input() icon:string = ''
  @Input() title:string = 'z'
  @Output() iconClick = new EventEmitter();
  @Output() toggle = new EventEmitter();

  @Input() opened = false;

  constructor(public accordion: AccordionComponent) {
    console.log(accordion.icon)
  }
}
