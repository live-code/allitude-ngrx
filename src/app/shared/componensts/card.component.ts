import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StopPropagationDirective } from '../stop-propagation.directive';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule, StopPropagationDirective],
  template: `
    <div>
      <div 
        class="bg-orange-400 text-white flex justify-between p-3"
        (click)="toggle()"
      >
        <div>{{title}}</div>
        <div
          (click)="iconClick.emit()"
          appStopPropagation
          *ngIf="icon">{{icon}}</div>
      </div>
      <div *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>

  `,
  styles: [`
    .title {
      background: black ; color: white
    }
  `]
})
export class CardComponent {
  @Input() icon:string = ''
  @Input() title:string = ''
  @Output() iconClick = new EventEmitter();

  @Input() opened = true;
  @Output() openedChange = new EventEmitter();


  toggle() {
    this.opened = !this.opened;
    this.openedChange.emit(this.opened)
  }

  ngOnChanges() {
    console.log(this.title)
  }
}
