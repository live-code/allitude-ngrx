import { Component, EventEmitter, Input, Output, Type, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { PanelComponent } from './panel/panel.component';

export type LoaderProps = {
  component: 'card' | 'panel',
  inputs: any,
  events: string[]
}

@Component({
  selector: 'app-loader',
  standalone: true,
  imports: [CommonModule],
  template: `
  `,
  styles: [
  ]
})
export class LoaderComponent {
  @Input({ required: true }) cfg!: LoaderProps;
  @Output() onEvent = new EventEmitter<{ compo: string, event: string }>()

  constructor(
    private view: ViewContainerRef
  ) {

  }
  ngOnChanges() {
    this.view.clear()
    const compo = this.view.createComponent<any>(
      COMPONENTS[this.cfg.component]
    )

    for (let key in this.cfg.inputs) {
      compo.setInput(key, this.cfg.inputs[key])
    }

    if (!this.cfg.events) return

    this.cfg.events
      .forEach((event) => {
        compo.instance[event]
          .subscribe(() => {
            this.onEvent.emit({
              compo: this.cfg.component,
              event: event
            })
          })
      })
   /* compo.setInput('title', 'fabio')
    compo.setInput('icon', '♥️')
    compo.instance.iconClick
      .subscribe(res => console.log('click', res))*/
    // compo.instance.title = 'ciao'

  }
}

const COMPONENTS: { [key: string]: Type<any>  } = {
  'card': CardComponent,
  'panel': PanelComponent
}

