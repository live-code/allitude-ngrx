import { LatLngExpression } from 'leaflet';
import * as L from 'leaflet'
import { AfterViewInit, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-leaflet',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLElement>
  @Input({ required: true}) coords!: LatLngExpression;
  @Input() zoom: number = 5;
  map!: L.Map;

  ngOnChanges(changes: SimpleChanges) {
    if (!this.map) {
      this.initMap()
    }

    const { zoom, coords } = changes;
    if (zoom) {
      this.map.setZoom(zoom.currentValue)
    }

    if (coords) {
      this.map.setView(this.coords)
    }
  }

  initMap(): void {
    this.map = L.map(this.host.nativeElement).setView(this.coords, this.zoom);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

  }

}
