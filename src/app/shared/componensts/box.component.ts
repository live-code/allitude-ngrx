import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-box',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div style="background: red; width: 100px; height: 100px">
      box works!
    </div>
  `,
  styles: [
  ]
})
export class BoxComponent {

}
