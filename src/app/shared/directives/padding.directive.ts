import {
  booleanAttribute,
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  numberAttribute, Renderer2
} from '@angular/core';
import { AuthService } from '../../core/auth.service';

export type PaddingSize = 'sm' | 'xl';


@Directive({
  selector: '[appPadding]',
  standalone: true
})
export class PaddingDirective {
  @Input({ alias: 'appPadding'}) set size(val:PaddingSize) {
    console.log('render', this.authService.auth$.getValue())
    this.renderer.removeClass(this.el.nativeElement, 'padding-sm')
    this.renderer.removeClass(this.el.nativeElement, 'padding-xl')
    this.renderer.addClass(
      this.el.nativeElement,
      val === 'sm' ?
        'padding-sm' :
        'padding-xl'
      )
  }
  // @Input({ required: true}) title = 'ciao'
  @Input({ transform: booleanAttribute}) showIcons: boolean = false;

  @Input({ transform: numberAttribute}) totalItems!: number;
  @Input({ transform: (val: number) => {
    return val * 1000
  }}) total!: number;

  @HostBinding() get class() {
    console.log(this.authService.auth$.getValue())
    // if (this.disable) ....
    return this.size === 'sm' ?
      'padding-sm' :
      'padding-xl'
  }


  @HostBinding() innerHTML = 'xyz'


  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private authService: AuthService
  ) {
  }
  ngOnInit() {
    console.log('pad', this.size)
  }

}
