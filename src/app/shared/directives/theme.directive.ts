import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appTheme]',
  standalone: true
})
export class ThemeDirective {
  @Input() appTheme: 'dark' | 'light' = 'light';

  @HostBinding() get class() {
    return this.appTheme
  }
  constructor() { }

}
