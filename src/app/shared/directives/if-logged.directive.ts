import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { combineLatest, distinct, distinctUntilChanged, map, tap, withLatestFrom } from 'rxjs';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {
  @Input({ alias: 'appIfLogged'}) requiredRole: string | undefined;

  constructor(
    private tpl: TemplateRef<any>,
    private el: ElementRef,
    private view: ViewContainerRef,
    private auth: AuthService
  ) {
  }

  ngOnInit() {
    this.auth.isLogged$()
      .pipe(
        distinctUntilChanged(),
        withLatestFrom(this.auth.role$()),
      )
      .subscribe(([isLogged, role]) => {
        if (isLogged && role === this.requiredRole) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })

  }

  ngOnInit2() {
    this.auth.auth$
      .subscribe(res => {
        if (res?.role === this.requiredRole) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
  }


}
