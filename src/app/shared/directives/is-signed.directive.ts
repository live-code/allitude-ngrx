import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIsSigned]',
  standalone: true
})
export class IsSignedDirective {

  @HostBinding() get hidden() {
    // if (this.disable) ....
    return !this.authService.auth$.getValue()
  }

  constructor(
    private authService: AuthService
  ) {}
}
