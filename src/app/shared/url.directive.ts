import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]',
  standalone: true
})
export class UrlDirective {
  @Input() appUrl: string = '';

  @HostListener('click')
  clickMe() {
    window.open(this.appUrl)
  }

}
