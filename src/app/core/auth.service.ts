import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';

export type Auth = {
  token: string;
  role: string;
  displayName: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null);

  constructor() {
  /*  this.login()

    setTimeout(() => {
      this.logout()
    }, 2000)



    setTimeout(() => {
      this.login()
    }, 4000)*/
  }

  login() {
    this.auth$.next({
      token: 'weifewoifeiowhf',
      displayName: 'fab',
      role: 'admin'
    })
  }

  logout() {
    this.auth$.next(null)
  }

  isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth )
      )
  }

  role$(): Observable<string | null> {
    return this.auth$
      .pipe(
        map(auth => auth ? auth.role : null )
      )
  }
}
