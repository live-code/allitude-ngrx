import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';


export const RouterActions = createActionGroup({
  source: 'Router',
  events: {
    'go': props<{ path: string }>(),
    'back': emptyProps,
    'forward': emptyProps,
  }
})

export const SyncStorage = createAction('sync storage')
