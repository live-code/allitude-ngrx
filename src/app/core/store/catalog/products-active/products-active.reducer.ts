import { createFeature, createReducer, on } from '@ngrx/store';
import { AppState } from '../../../../app.config';
import { Product } from '../../../../model/product';
import { ProductsActiveActions } from './products-active.actions';

export interface ProductsActiveState {
  active: Product | null
}

const initialState: ProductsActiveState = {
  active: null
}

export const productActiveReducer = createReducer(
  initialState,
  on(ProductsActiveActions.clearActive, (state, action) => ({...state, active: null })),
  on(ProductsActiveActions.setActive, (state, action) => ({...state, active: action.item })),
)

export const selectActive = (store:AppState) => store.catalog.activeProduct.active

/*
export const productsActiveFeature = createFeature({
  name: 'products active',
  reducer: createReducer(
    initialState,
    on(ProductsActiveActions.clearActive, (state, action) => ({...state, active: null })),
    on(ProductsActiveActions.setActive, (state, action) => ({...state, active: action.item })),
  )
})


export const {
  selectActive
} = productsActiveFeature*/

// reducer
/*
export const productsReducer = createReducer(
  initialState,
  on(
    ProductsActions.loadSuccess,
    (state, action) => [...action.items]
  )
)
*/
