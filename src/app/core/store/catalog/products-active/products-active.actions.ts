import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';
import { Product } from '../../../../model/product';

export const ProductsActiveActions = createActionGroup({
  source: 'Products Active',
  events: {
    'setActive': props<{ item: Product}>(),
    'clearActive': emptyProps
  }
})
