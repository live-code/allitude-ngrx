import { createSelector } from '@ngrx/store';
import { AppState } from '../../../../app.config';
import { selectUrl } from '../../router/router.selector';

export const getProducts = (state: AppState) => state.catalog.products.list
export const getProductsError = (state: AppState) => state.catalog.products.error

export const getSomething = createSelector(
  getProducts,
  selectUrl,
  (products, url) => products.length + url 
)
