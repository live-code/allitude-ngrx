import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';
import { Product } from '../../../../model/product';

export const ProductsActions = createActionGroup({
  source: 'Products',
  events: {
    // ...
    'load': emptyProps(),
    'load success': props<{ items: Product[] }>(),
    'load fail': emptyProps,
    // add
    'add': props<{item: Product}>(),
    'add success': props<{ items: Product[] }>(),
    'add fail': emptyProps,
    // ...
    'delete': props<{ id: number }>(),
    'delete success': props<{ id: number }>(),
    'delete fail': emptyProps,
  }
})

export const SyncStorage = createAction('sync storage')
