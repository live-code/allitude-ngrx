import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { catchError, concatMap, EMPTY, exhaustMap, map, mergeMap, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Product } from '../../../../model/product';
import { RouterActions } from '../../router/router.actions';
import { selectUrl } from '../../router/router.selector';
import { selectActive } from '../products-active/products-active.reducer';
import { ProductsActions, SyncStorage } from './products.actions';

export const syncStorage = createEffect((
  actions$ = inject(Actions),
  http = inject(HttpClient),
  store = inject(Store)
) => {
  // outer obs
  return actions$
    .pipe(
      ofType(ROOT_EFFECTS_INIT),
      tap(() => {
        const dataFromStorage = sessionStorage.getItem('items');
        if (dataFromStorage) {
          const items = JSON.parse(dataFromStorage)
          store.dispatch(ProductsActions.loadSuccess({ items }))
        }
      })
    )
}, { functional: true, dispatch: false })


export const loadProducts = createEffect((
  actions$ = inject(Actions),
  http = inject(HttpClient),
  store = inject(Store)
) => {
  // outer obs
  return actions$
    .pipe(
      ofType(ProductsActions.load),
      withLatestFrom(store.select(selectUrl)),
      // inner obs
      exhaustMap(
        ([action, url]) => http.get<Product[]>('http://localhost:3000/products' )
          .pipe(
            map((items) => {
              sessionStorage.setItem('items', JSON.stringify(items))
              return ProductsActions.loadSuccess({ items })
            }),
            catchError(() => of(ProductsActions.loadFail()))
          )
      ),
    )
}, { functional: true })


export const deleteProduct = createEffect((
  actions$ = inject(Actions),
  http = inject(HttpClient),
  store = inject(Store)
) => {
  // outer obs
  return actions$
    .pipe(
      ofType(ProductsActions.delete),
      // inner obs
      exhaustMap(
        ({ id }) => http.delete(`http://localhost:3000/products/${id}`)
          .pipe(
            map(() => ProductsActions.deleteSuccess({ id })),
            catchError(() => of(ProductsActions.deleteFail()))
          )
      ),
    )
}, { functional: true })


export const deleteProductSuccess = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
  store = inject(Store),
) => {
  // outer obs
  return actions$
    .pipe(
      ofType(ProductsActions.deleteSuccess),
      withLatestFrom(store.select(selectUrl)),
      map(() => RouterActions.go({ path: 'demo2' }))
    )
}, { functional: true })



/*


export const deleteProductSuccess = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
  store = inject(Store),
) => {
  // outer obs
  return actions$
    .pipe(
      ofType(ProductsActions.deleteSuccess),
      withLatestFrom(store.select(selectUrl)),
      tap(([action, url]) => {
        router.navigateByUrl('demo2')
      })
    )
}, { functional: true, dispatch: false })
*/

