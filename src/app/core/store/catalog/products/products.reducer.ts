import { createFeature, createReducer, on } from '@ngrx/store';
import { AppState } from '../../../../app.config';
import { Product } from '../../../../model/product';
import { ProductsActiveActions } from '../products-active/products-active.actions';
import { ProductsActions } from './products.actions';

export interface ProductsState {
  list: Product[],
  error: boolean;
  pending: boolean;
}

const initialState: ProductsState = {
  list: [],
  error: false,
  pending: false,
}


export const productsReducer = createReducer(
  initialState,
  on(ProductsActions.load, (state, action) => ({...state, pending: true})),
  on(ProductsActions.loadSuccess, (state, action) => ({...state, list: action.items, pending: false})),
  on(ProductsActions.loadFail, (state, action) => ({...state, list: [], error: true, pending: false})),

  on(ProductsActions.deleteSuccess, (state, action) => ({
    ...state,
    list: state.list.filter(item => item.id !== action.id),
    pending: false
  })),

)

/*


export const productsFeature = createFeature({
  name: 'products',
  reducer: createReducer(
    initialState,
    on(ProductsActions.load, (state, action) => ({...state, pending: true})),
    on(ProductsActions.loadSuccess, (state, action) => ({...state, list: action.items, pending: false})),
    on(ProductsActions.loadFail, (state, action) => ({...state, list: [], error: true, pending: false}))
  )
})
export const {
  selectList,
  selectError,
  selectPending,
} = productsFeature*/

// reducer
/*
export const productsReducer = createReducer(
  initialState,
  on(
    ProductsActions.loadSuccess,
    (state, action) => [...action.items]
  )
)
*/
