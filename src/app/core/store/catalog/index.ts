import { ActionReducer, ActionReducerMap, combineReducers } from '@ngrx/store';
import {
  productActiveReducer,
  ProductsActiveState
} from './products-active/products-active.reducer';
import { productsReducer, ProductsState } from './products/products.reducer';

export interface CatalogState {
  products: ProductsState;
  activeProduct: ProductsActiveState
}


export const catalogReducer = combineReducers({
  products: productsReducer,
  activeProduct: productActiveReducer
})
