import { createEffect } from '@ngrx/effects';
import { createAction } from '@ngrx/store';
import { debounceTime, fromEvent, map, tap } from 'rxjs';
import { resize } from './utils.actions';

export const resizeEffect$ = createEffect((
) => {
  return fromEvent(window, 'resize')
    .pipe(
      debounceTime( 1000 ),
      map(() => resize())
    )
}, { functional: true });


