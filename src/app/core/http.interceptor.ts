import { inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { mergeMap, Observable, withLatestFrom } from 'rxjs';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {
  store = inject(Store)
  constructor() {}
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
  }

  /*
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return this.store.select(selectToken)
      .pipe(
        withLatestFrom(this.store.select(pippo))
        mergeMap(([token, pippo]) => {
          return iif(
            () => condition,
            next.handle(clone),
            next.handle(request)
          )

        })
      )


  }
  */
}
