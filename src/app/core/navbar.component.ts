import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { IsSignedDirective } from '../shared/directives/is-signed.directive';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, RouterLink, IsSignedDirective, IfLoggedDirective],
  template: `
    
    <div class="flex flex-col  sm:flex-row flex-wrap gap-3 justify-center">
      <button routerLink="demo1">demo1</button>
      <button routerLink="demo2" class="primary">demo2</button>
      <button routerLink="demo3">demo3</button>
      <button routerLink="demo4">demo4</button>
      <button routerLink="demo5">demo5</button>
      <button routerLink="demo6">demo6</button>
    </div>
    
    <button *appIfLogged="'admin'">PRIVATE PAGE 1</button>
    <button appIsSigned>PRIVATE PAGE 2 </button>
    <button *ngIf="authServ.auth$ | async">PRIVATE PAGE 3</button>
    <hr>
  `,
})
export class NavbarComponent {
  authServ = inject(AuthService)

}
