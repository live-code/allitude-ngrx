import { provideHttpClient } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideRouterStore, routerReducer, RouterReducerState } from '@ngrx/router-store';
import { provideStoreDevtools } from '@ngrx/store-devtools';

import { routes } from './app.routes';
import { ActionReducerMap, provideStore } from '@ngrx/store';
import { provideEffects } from '@ngrx/effects';
import { catalogReducer, CatalogState } from './core/store/catalog';
import * as productsEffects from './core/store/catalog/products/products.effect';
import * as routerEffects from './core/store/router/router.effects';
import * as utilsEffects from './core/store/utils/utls.effects';

export interface AppState {
  catalog: CatalogState
  todos: number;
  router: RouterReducerState
}

export const reducers: ActionReducerMap<AppState> = {
  catalog: catalogReducer,
  todos: () => 123,
  router: routerReducer
}

export const appConfig: ApplicationConfig = {
  providers: [
    provideHttpClient(),
    provideRouter(routes),
    provideStore(reducers),
    provideStoreDevtools({ maxAge: 30 }),
    provideRouterStore(),
    provideEffects([productsEffects, routerEffects, utilsEffects])
]
};
