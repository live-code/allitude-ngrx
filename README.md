NEXT in LAST LESSON
* directive strutturali
  * #template reference variabiles
  * ng-template
  * view container ref
* ngComponentOutlet
* Tailwind
* Componenti
  * Card
    * + Card Compound
    * ng-content
    * With Animations
  * Accordion 
  * LeafLet
* Directive Composition API
* Custom Pipe??
* Signal
* Cypress?
* Control Flow e Defer Angular 17



NGRX

• component store

• router effect
• gestione errori
• create selector
• signal (with and withour NGRX)

• Component reusable / ...
• Custom Directive  <div *ifLogged>...  <div *isRole="admin">...
• Custom Pipe
• Angular 17 news

----

• RxJS avanzato:
- comprendere bene i flatterning operator (switchMap, mergeMap ecc.)
- creazione sequenze di observable (migliorare il concatenamento di observable, nell'ottica di migliorare i servizi con nested call, nested observable e negli effect di NGRX)
- evitare promise
- multicasting vs unicast (Subjects e operatori share, shareReplay, ...)

• NGRX
- gestione side effects (HTTP, localstorage, routing ecc.)
- creazione store e azioni per il router in modo da tracciarli
- astrazione di azioni molto simili (ad es. CRUD ed Errori)
- migliorare nell'utilizzo di store e selettori (anche sfruttando il pipe async)
- Component Store

• APPROFONDIMENTI
- ChangeDetection e performance
- Attribute e Structural Direttives
- Custom Pipe (async e sync)
- Components (diverse tecniche legate alla composizione e decoratori ViewChildren, ContentChildre, ecc.)
- Testing con Storybook
- Testing con Cypress
